       identification division.
       program-id. DataInitCheck.

       data division.
       WORKING-STORAGE SECTION.
         EJECT
       01  TAB-POS.
           02  FILLER  PIC A(14) VALUE 0.  *> Noncompliant
           02  FILLER  PIC 9(14) VALUE 'ASDFJKL;QWERTY'.  *> Noncompliant

       01 MYGROUP PIC 9(1).
          88 X VALUE 1,2.
          88 Y VALUE 3, "BLUE".  *> Noncompliant; BLUE is alphanumeric