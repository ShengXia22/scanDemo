DATA DIVISION.
   WORKING-STORAGE SECTION.
   
   EXEC SQL
   INCLUDE table-name
   END-EXEC.
   EXEC SQL
   INSERT INTO MY_TABLE  *> Noncompliant; N2 value omitted
   (
       N1
   )
   VALUES
   (
       :ITQ1-NUMBER,
   )
   END-EXEC.

   EXEC SQL BEGIN DECLARE SECTION
   END-EXEC.
   
   01 STUDENT-REC.
      05 STUDENT-ID PIC 9(4).
      05 STUDENT-NAME PIC X(25).
      05 STUDENT-ADDRESS X(50).
   EXEC SQL END DECLARE SECTION
   END-EXEC.

   EXEC SQL
    CREATE TABLE INVENTORY
                    (PARTNO         SMALLINT     NOT NULL,
                    DESCR          VARCHAR(24 ),
                    PRIMARY KEY(PARTNO))
    END-EXEC.

    EXEC SQL
    DROP TABLE EMPLOYEE RESTRICT
    END-EXEC.

    EXEC SQL
    ALTER TABLE EQUIPMENT
        DROP COLUMN LOCATION CASCADE
    END-EXEC.

    
    CREATE table my_table (
        column_a integer GENERATED ALWAYS AS IDENTITY primary key not null,
        column_b varchar(50)
    );

    INSERT into my_table (column_a, column_b)
    VALUES (1, 'Hello World');  -- Noncompliant

    UPDATE USERS
    SET USER_ID = :new-id, USER_NAME = :new-name  *> Noncompliant
    WHERE USER_ID = :input


    SELECT name
    FROM product
    WHERE name LIKE 'choc'


    SELECT *
    FROM fruit
    WHERE type='apple' AND type='orange'  -- Noncompliant


    SELECT * FROM MY_TABE WHERE C2 = C3 + :HostVar1  -- Noncompliant

    SELECT * FROM MY_TABLE WHERE YEAR(BIRTHDATE) > 2000  -- Noncompliant

    UPDATE USERS
    SET USER_ID = :new-id, USER_NAME = :new-name  *> Noncompliant
    WHERE USER_ID = :input


    SELECT fname, lname, city
    FROM people
    WHERE city IS NOT NULL
    FETCH FIRST 10 ROWS ONLY; -- Noncompliant selects 10 random rows