   WORKING-STORAGE SECTION.
         EJECT
       01  TAB-POS.
           02  FILLER  PIC A(14) VALUE 0.  *> Noncompliant
           02  FILLER  PIC 9(14) VALUE 'ASDFJKL;QWERTY'.  *> Noncompliant

       01 MYGROUP PIC 9(1).
          88 X VALUE 1,2.
          88 Y VALUE 3, "BLUE".  *> Noncompliant; BLUE is alphanumeric

01 NUM-A   PIC 9(2)V9.
*> ...

    MOVE 88.89   TO NUM-A  *> Noncompliant. Becomes 88.8
    MOVE 178.7   TO NUM-A  *> Noncompliant. Becomes 78.7
    MOVE 999.99 TO NUM-A  *> Noncompliant. Truncated on both ends; becomes 99.9


01  MY-STR          PIC X(3) VALUE SPACES.
01  MY-NUM         PIC 9(3) VALUE ZEROES.
*> ...
    MOVE '1'         TO MY-STR
    MOVE MY-STR  TO MY-NUM  *> Noncompliant

  PERFORM SECOND-P THRU FIRST-P.
  ...

 FIRST-P.
   ...

 SECOND-P.
   ...

EVALUATE X *> Noncompliant
  WHEN 1
    PERFORM SECTION1
  WHEN OTHER
    PERFORM SECTION1
END-EVALUATE.

IF X = 1 THEN *> Noncompliant
  PERFORM SECTION1
ELSE
  PERFORM SECTION1
END-IF.